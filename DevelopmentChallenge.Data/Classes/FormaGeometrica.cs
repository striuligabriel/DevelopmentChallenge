﻿/******************************************************************************************************************/
/******* ¿Qué pasa si debemos soportar un nuevo idioma para los reportes, o agregar más formas geométricas? *******/
/******************************************************************************************************************/

/*
 * TODO: 
 * Refactorizar la clase para respetar principios de la programación orientada a objetos.
 * Implementar la forma Trapecio/Rectangulo. 
 * Agregar el idioma Italiano (o el deseado) al reporte.
 * OPCIONAL: Se agradece la inclusión de nuevos tests unitarios para validar el comportamiento de la nueva funcionalidad agregada 
 * (los tests deben pasar correctamente al entregar la solución, incluso los actuales.)
 * Una vez finalizado, hay que subir el código a un repo GIT y ofrecernos la URL para que podamos utilizar la nueva versión :).
 */

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace DevelopmentChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        public abstract string Nombre { get; }
        public abstract decimal CalcularArea();
        public abstract decimal CalcularPerimetro();

        private static readonly Dictionary<string, string> idiomaCache = new Dictionary<string, string>();


        public static string Imprimir(List<FormaGeometrica> formas, string idioma)
        {
            var idiomaJson = ObtenerIdiomaJson(idioma);
            var sb = new StringBuilder();

            if (!formas.Any())
            {
                sb.Append(ObtenerMensaje(idiomaJson, "EmptyListMessage"));
            }
            else
            {

                // HEADER
                sb.Append(ObtenerMensaje(idiomaJson, "ReportHeader"));

                var formaData = new Dictionary<string, FormaData>();

                foreach (var forma in formas)
                {
                    var nombre = forma.Nombre;

                    if (!formaData.ContainsKey(nombre))
                    {
                        formaData[nombre] = new FormaData();
                    }

                    var data = formaData[nombre];
                    data.Cantidad++;
                    data.AreaTotal += forma.CalcularArea();
                    data.PerimetroTotal += forma.CalcularPerimetro();
                }

                foreach (var kvp in formaData)
                {
                    sb.Append(ObtenerLinea(kvp.Value.Cantidad, kvp.Value.AreaTotal, kvp.Value.PerimetroTotal, kvp.Key, idiomaJson));
                }

                // FOOTER
                sb.Append(ObtenerMensaje(idiomaJson, "TotalHeader"));
                sb.Append(formas.Count + " " + ObtenerMensaje(idiomaJson, "TotalForms") + " ");
                sb.Append(ObtenerMensaje(idiomaJson, "TotalPerimeter") + formas.Sum(f => f.CalcularPerimetro()).ToString("#.##", CultureInfo.InvariantCulture) + " ");
                sb.Append(ObtenerMensaje(idiomaJson, "TotalArea") + formas.Sum(f => f.CalcularArea()).ToString("#.##", CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, string forma, string idiomaJson)
        {
            if (cantidad <= 0)
                return string.Empty;

            var formaTraducida = ObtenerMensaje(idiomaJson, forma);

            var sb = new StringBuilder();
            sb.Append(cantidad).Append(" ").Append(formaTraducida).Append(" | ");
            sb.Append(ObtenerMensaje(idiomaJson, "Area")).Append(" ").Append(area.ToString("#.##", CultureInfo.InvariantCulture)).Append(" | ");
            sb.Append(ObtenerMensaje(idiomaJson, "Perimeter")).Append(perimetro.ToString("#.##", CultureInfo.InvariantCulture)).Append(" <br/>");

            return sb.ToString();

        }

        private static string ObtenerMensaje(string idiomaJson, string clave)
        {
            var idioma = JsonConvert.DeserializeObject<Dictionary<string, string>>(idiomaJson);
            return idioma.ContainsKey(clave) ? idioma[clave] : clave;
        }

        private static string ObtenerIdiomaJson(string idioma)
        {
            var idiomaArchivo = string.IsNullOrEmpty(idioma) ? FormaGeometricaIdioma.Castellano : idioma;

            if (idiomaCache.ContainsKey(idiomaArchivo))
            {
                return idiomaCache[idiomaArchivo];
            }

            var rutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Idiomas", $"Idioma-{idiomaArchivo}.json");
            var idiomaJson = File.ReadAllText(rutaArchivo);
            idiomaCache[idiomaArchivo] = idiomaJson;

            return idiomaJson;
        }

    }

    // Resto del código de las clases derivadas y constantes...

    public class FormaData
    {
        public int Cantidad { get; set; }
        public decimal AreaTotal { get; set; }
        public decimal PerimetroTotal { get; set; }
    }

    public class FormaGeometricaIdioma
    {
        public const string Castellano = "es-ES";
        public const string Ingles = "us-US";
        public const string Frances = "fr-Fr";
        public const string Italiano = "il-Il";
    }

    public class Cuadrado : FormaGeometrica
    {
        public override string Nombre => "Cuadrado";

        private readonly decimal _lado;

        public Cuadrado(decimal lado)
        {
            _lado = lado;
        }

        public override decimal CalcularArea()
        {
            return _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 4;
        }
    }

    public class Circulo : FormaGeometrica
    {
        public override string Nombre => "Circulo";

        private readonly decimal _radio;

        public Circulo(decimal radio)
        {
            _radio = radio;
        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (_radio * _radio);
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * (decimal)Math.PI * _radio;
        }
    }

    public class TrianguloEquilatero : FormaGeometrica
    {
        public override string Nombre => "TrianguloEquilatero";

        private readonly decimal _lado;

        public TrianguloEquilatero(decimal lado)
        {
            _lado = lado;
        }

        public override decimal CalcularArea()
        {
            return ((_lado * _lado) * (decimal)Math.Sqrt(3)) / 4;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 3;
        }
    }


    public class TrapecioRectangulo : FormaGeometrica
    {
        public override string Nombre => "TrapecioRectangulo";
        private readonly decimal _lado1;
        private readonly decimal _lado2;
        private readonly decimal _baseMayor;
        private readonly decimal _baseMenor;

        public TrapecioRectangulo(decimal lado1, decimal lado2, decimal baseMayor, decimal baseMenor)
        {
            _lado1 = lado1;
            _lado2 = lado2;
            _baseMayor = baseMayor;
            _baseMenor = baseMenor;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado1 + _lado2 + _baseMayor + _baseMenor;
        }

        public override decimal CalcularArea()
        { 
            decimal altura = (decimal)Math.Sqrt((double)(_lado1 * _lado1 - (((_baseMayor - _baseMenor) * (_lado2 - _lado1)) / (_baseMayor - _lado1)) * (((_baseMayor - _baseMenor) * (_lado2 - _lado1)) / (_baseMayor - _lado1))));

            return ((_baseMayor + _baseMenor) * altura) / 2;
        }
    }

    public class Rectangulo : FormaGeometrica
    {
        public override string Nombre => "Rectangulo";

        private readonly decimal _base;
        private readonly decimal _altura;

        public Rectangulo(decimal b, decimal a)
        {
            _base = b;
            _altura = a;
        }

        public override decimal CalcularArea()
        {
            return _base * _altura;
        }

        public override decimal CalcularPerimetro()
        {
            return 2 * (_base + _altura);            
        }
    }
}
