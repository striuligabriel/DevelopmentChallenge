﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DevelopmentChallenge.Data.Classes;
using NUnit.Framework;

namespace DevelopmentChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {

        
        [TestCase(FormaGeometricaIdioma.Ingles, "<h1>Empty list of shapes!</h1>")]
        [TestCase(FormaGeometricaIdioma.Castellano, "<h1>Lista vacía de formas!</h1>")]
        [TestCase(FormaGeometricaIdioma.Italiano, "<h1>Elenco di moduli video!</h1>")]
        [TestCase(FormaGeometricaIdioma.Frances, "<h1>Liste de formes vide !</h1>")]
        public void Imprimir_DebeRetornarMensajeListaVacia_WhenListaFormasVacia(string idioma, string resultadoEsperado)
        {
            List<FormaGeometrica> formas = new List<FormaGeometrica>();

            string resultado = FormaGeometrica.Imprimir(formas, idioma);

            Assert.That(resultado, Is.EqualTo(resultadoEsperado));
        }
         
        [Test]
        public void Imprimir_DebeRetornarInformeCorrecto_WhenListaFormasNoVacia()
        {
            List<FormaGeometrica> formas = new List<FormaGeometrica>
            {
                new Cuadrado(2),
                new Circulo(3),
                new TrianguloEquilatero(4)
            };

            string resultado = FormaGeometrica.Imprimir(formas, FormaGeometricaIdioma.Castellano);

            Assert.That(resultado, Is.EqualTo("<h1>Reporte de Formas</h1>1 Cuadrados | Área 4 | Perímetro 8 <br/>1 Círculos | Área 28.27 | Perímetro 18.85 <br/>1 Triángulos | Área 6.93 | Perímetro 12 <br/>TOTAL:<br/>3 formas Perimetro 38.85 Area 39.2"));
        }
         
        [Test]
        public void Imprimir_DebeRetornarInformeCorrecto_WhenListaFormasNoVacia_FormaCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> { new Cuadrado(5)  };

            var resumen = FormaGeometrica.Imprimir(cuadrados, FormaGeometricaIdioma.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrados | Área 25 | Perímetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [Test]
        public void Imprimir_DebeRetornarInformeCorrecto_WhenListaFormasNoVacia_FormasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3),

            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, FormaGeometricaIdioma.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase(FormaGeometricaIdioma.Ingles, "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 52.03 | Perimeter 36.13 <br/>3 Triangles | Area 49.64 | Perimeter 51.6 <br/>TOTAL:<br/>7 shapes Perimeter 115.73 Area 130.67")]
        [TestCase(FormaGeometricaIdioma.Castellano, "<h1>Reporte de Formas</h1>2 Cuadrados | Área 29 | Perímetro 28 <br/>2 Círculos | Área 52.03 | Perímetro 36.13 <br/>3 Triángulos | Área 49.64 | Perímetro 51.6 <br/>TOTAL:<br/>7 formas Perimetro 115.73 Area 130.67")]
        public void Imprimir_DebeRetornarInformeCorrecto_WhenListaFormasNoVacia_Formas(string idioma, string resultadoEsperado)
        {
            var formas = new List<FormaGeometrica>
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, idioma);

            Assert.AreEqual(resultadoEsperado, resumen);
        }
         
        [Test]
        public void Imprimir_DebeRetornarResultadoCorrecto_WhenListaFormasGrande()
        {
            // Arrange
            List<FormaGeometrica> formas = new List<FormaGeometrica>();

            for (int i = 0; i < 10000; i++)
            {
                formas.Add(new Cuadrado(i));
                formas.Add(new Circulo(i));
                formas.Add(new TrianguloEquilatero(i));
            }

            // Act
            string resultado = FormaGeometrica.Imprimir(formas, FormaGeometricaIdioma.Castellano);

            // Assert
            Assert.That(resultado, Is.Not.Empty);
        }

        [Test]
        public void Imprimir_DebeEjecutarseEnTiempoRazonable_WhenListaFormasGrande()
        {
            // Arrange
            List<FormaGeometrica> formas = new List<FormaGeometrica>();

            for (int i = 0; i < 10000; i++)
            {
                formas.Add(new Cuadrado(i));
                formas.Add(new Circulo(i));
                formas.Add(new TrianguloEquilatero(i));
            }

            var idioma = FormaGeometricaIdioma.Castellano;

            // Act
            var stopwatch = Stopwatch.StartNew();
            string resultado = FormaGeometrica.Imprimir(formas, idioma);
            stopwatch.Stop();

            Console.WriteLine($"Tiempo de ejecución: {stopwatch.ElapsedMilliseconds} ms");

            // Assert
            Assert.That(stopwatch.ElapsedMilliseconds, Is.LessThan(1000));
        }



        [Test]
        public void Imprimir_DebeRetornarInformeCorrecto_NuevasFormas()
        {
            List<FormaGeometrica> formas = new List<FormaGeometrica>
            {
                new Rectangulo(2, 3),
                new TrapecioRectangulo(2, 2, 4, 6)
            };

            string resultado = FormaGeometrica.Imprimir(formas, FormaGeometricaIdioma.Castellano);

            Assert.That(resultado, Is.EqualTo("<h1>Reporte de Formas</h1>1 Rectángulos | Área 6 | Perímetro 10 <br/>1 Trapecios Rectángulos | Área 10 | Perímetro 14 <br/>TOTAL:<br/>2 formas Perimetro 24 Area 16"));
        }
    }
}
